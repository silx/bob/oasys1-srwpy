# oasys1-srwpy

[![pipeline status](https://gitlab.esrf.fr/silx/bob/oasys1-srwpy/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/oasys1-srwpy/pipelines)

Build binary packages for [oasys1-srwpy](https://github.com/oasys-kit/oasys1-srwpy): [Artifacts](https://silx.gitlab-pages.esrf.fr/bob/oasys1-srwpy)
